select
 s.ip
from
  (select l.ip, count(*) numReq from wallethub.Log l where l.date between '2017-01-01 13:00:00' and '2017-01-01 14:00:00' group by l.ip) s
where
  s.numReq > 100;