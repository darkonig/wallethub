package com.ef;

import com.ef.db.DBUtil;
import com.ef.db.LogWorker;
import com.ef.exception.LogWorkerException;
import com.ef.model.BlockedIp;
import com.ef.model.Log;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.time.DateUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parser {

    public static void main(String[] args) {
        Parser parser = new Parser();
        parser.parser(args);
    }

    public void parser(String[] args) {
        Options options = new Options();

        Option optStartDate = new Option("s", "startDate", true, "Start date on format yyyy-MM-dd.HH:mm:ss");
        optStartDate.setRequired(true);
        options.addOption(optStartDate);

        Option optDuration = new Option("d", "duration", true, "Duration: \"hourly\", \"daily\"");
        optDuration.setRequired(true);
        options.addOption(optDuration);

        Option optThreshold = new Option("t", "threshold", true, "Threshold");
        optThreshold.setRequired(true);
        options.addOption(optThreshold);

        Option optAccesslog = new Option("l", "accesslog", true, "Absolute path of the file");
        optAccesslog.setRequired(true);
        options.addOption(optAccesslog);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Parser", options);

            System.exit(1);
            return;
        }

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");

        try {
            String file = cmd.getOptionValue("accesslog");
            Date startDate = df.parse(cmd.getOptionValue("startDate"));
            Duration duration = Duration.getDuration(cmd.getOptionValue("duration"));
            int threshold = Integer.parseInt(cmd.getOptionValue("threshold"));

            // read file
            Path path = Paths.get(file);
            try (Stream<String> lines = Files.lines(path);) {
                Stream<Log> logs = getLog(lines);

                List<BlockedIp> blockedIps = getBlockedIps(logs, startDate, duration, threshold);
                blockedIps.stream().forEach(System.out::println);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (IllegalArgumentException | java.text.ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Parser", options);

            System.exit(1);
            return;
        } catch (LogWorkerException e) {
            System.out.println(e.getMessage());
        }

        DBUtil.closeFactory();
    }

    public List<BlockedIp> getBlockedIps(Stream<Log> logs, Date startDate, Duration duration, int threshold) throws LogWorkerException {
        Date endDate = getEndDate(startDate, duration);

        LogWorker finder = new LogWorker();
        // import to database
        finder.importLog(logs);

        // query for the values
        List<String> ips = finder.getIps(startDate, endDate, threshold);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
        String formattedStart = df.format(startDate);
        String formattedEnd = df.format(endDate);

        // save blocked ips
        List<BlockedIp> blocked = ips.stream()
                .map(ip -> new BlockedIp(ip,
                                String.format("Blocked because made %s requests between %s and %s."
                                        , threshold, formattedStart, formattedEnd)
                        )
                ).collect(Collectors.toList());
        finder.saveBlockerIps(blocked);

        return blocked;
    }

    /**
     * Convert lines to log
     *
     * @param lines
     * @return
     */
    private Stream<Log> getLog(Stream<String> lines) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return lines.map(line -> {
            String[] s = line.split("\\|");
            Log l = new Log();
            try {
                l.setDate(df.parse(s[0]));
                l.setIp(s[1]);
                l.setRequest(s[2]);
                l.setStatus(Short.parseShort(s[3]));
                l.setUserAgent(s[4]);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
                return null;
            }
            return l;
        });
    }

    /**
     * Get endDate based on a given duration
     *
     * @param startDate
     * @param duration
     * @return
     */
    private Date getEndDate(Date startDate, Duration duration) {
        if (duration == Duration.HOURLY) {
            return DateUtils.addHours(startDate, 1);
        }

        return DateUtils.addDays(startDate, 1);
    }

}
