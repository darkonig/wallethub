package com.ef;

import com.ef.db.DBUtil;
import com.ef.exception.LogWorkerException;
import com.ef.model.Log;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.time.DateUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParserJava {

    /**
     * Filter ips using java lambda.
     *
     * @param args
     * @throws ParseException
     * @throws URISyntaxException
     */
    public static void main(String[] args) throws ParseException, URISyntaxException {
        ParserJava parser = new ParserJava();
        parser.parser(args);
    }

    public void parser(String[] args) {
        Options options = new Options();

        Option optStartDate = new Option("s", "startDate", true, "Start date on format yyyy-MM-dd.HH:mm:ss");
        optStartDate.setRequired(true);
        options.addOption(optStartDate);

        Option optDuration = new Option("d", "duration", true, "Duration: \"hourly\", \"daily\"");
        optDuration.setRequired(true);
        options.addOption(optDuration);

        Option optThreshold = new Option("t", "threshold", true, "Threshold");
        optThreshold.setRequired(true);
        options.addOption(optThreshold);

        Option optAccesslog = new Option("l", "accesslog", true, "Absolute path of the file");
        optAccesslog.setRequired(true);
        options.addOption(optAccesslog);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (org.apache.commons.cli.ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Parser", options);

            System.exit(1);
            return;
        }

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");

        try {
            String file = cmd.getOptionValue("accesslog");
            Date startDate = df.parse(cmd.getOptionValue("startDate"));
            Duration duration = Duration.getDuration(cmd.getOptionValue("duration"));
            int threshold = Integer.parseInt(cmd.getOptionValue("threshold"));

            Date endDate = getEndDate(startDate, duration);

            // read file
            Path path = Paths.get(file);
            try (Stream<String> lines = Files.lines(path);) {
                // parse file
                Map<String, List<Log>> logs = getLog(lines)
                        .filter(log -> log.getDate().after(startDate) && log.getDate().before(endDate))
                        .collect(Collectors.groupingBy(Log::getIp));

                // get those ips who passed
                Map<String, List<Log>> result = new HashMap<>();
                logs.forEach((k,v) -> {
                    if (v.size() > threshold) {
                        result.put(k, v);
                    }
                });
                // print to console
                result.forEach((k,v) -> System.out.println(k + "\t- " + v.size()));
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (IllegalArgumentException | java.text.ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Parser", options);

            System.exit(1);
            return;
        }

        DBUtil.closeFactory();
    }

    public static Stream<Log> getLog(Stream<String> lines) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        return lines.map(line -> {
            String[] s = line.split("\\|");
            Log l = new Log();
            try {
                l.setDate(df.parse(s[0]));
                l.setIp(s[1]);
                l.setRequest(s[2]);
                l.setStatus(Short.parseShort(s[3]));
                l.setUserAgent(s[4]);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
            return l;
        });
    }

    private Date getEndDate(Date startDate, Duration duration) {
        if (duration == Duration.HOURLY) {
            return DateUtils.addHours(startDate, 1);
        }

        return DateUtils.addDays(startDate, 1);
    }

}
