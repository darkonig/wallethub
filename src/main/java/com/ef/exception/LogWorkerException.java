package com.ef.exception;

public class LogWorkerException extends Exception {

    public LogWorkerException() {
        super();
    }

    public LogWorkerException(String message) {
        super(message);
    }

    public LogWorkerException(String message, Throwable cause) {
        super(message, cause);
    }

}
