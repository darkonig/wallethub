package com.ef.db;

import com.ef.model.BlockedIp;
import com.ef.model.Log;
import com.ef.exception.LogWorkerException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

public class LogWorker {

    /**
     * Import logs to the database
     *
     * @param log
     * @throws LogWorkerException
     */
    public void importLog(Stream<Log> log) throws LogWorkerException {
        QueryJob job = new QueryJob() {

            @Override
            public void execute(EntityManager manager) {
                manager .getTransaction().begin();
                log.forEach(l -> {
                    System.out.println(l);
                    manager.persist(l);
                });
                manager.getTransaction().commit();
            }

        };

        executeQuery(job);
    }

    /**
     * Gets all the ips between a two dates how made more than a given number of request (threshold)
     * @param startDate start date
     * @param endDate end date
     * @param threshold number of request
     * @return ips
     * @throws LogWorkerException
     */
    public List<String> getIps(Date startDate, Date endDate, int threshold) throws LogWorkerException {
        EntityManager manager = null;

        try {
            manager = DBUtil.createEntityManager();

            Query q = manager.createNativeQuery(
                    "select " +
                        " s.ip " +
                        "from " +
                        "  (select l.ip, count(*) numReq from LOG l where l.date between :start and :end group by l.ip) s " +
                        "where " +
                        "  s.numReq > :threshold");
            q.setParameter("start", startDate);
            q.setParameter("end", endDate);
            q.setParameter("threshold", threshold);

            return q.getResultList();
        } catch (Exception e) {
            throw new LogWorkerException(e.getMessage(), e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }

    /**
     * Saves the blocked ips
     *
     * @param ips
     * @throws LogWorkerException
     */
    public void saveBlockerIps(List<BlockedIp> ips) throws LogWorkerException {
        QueryJob job = new QueryJob() {

            @Override
            public void execute(EntityManager manager) {
                manager.getTransaction().begin();
                ips.forEach(ip -> manager.persist(ip));
                manager.getTransaction().commit();
            }

        };

        executeQuery(job);
    }

    private void executeQuery(QueryJob executor) throws LogWorkerException {
        EntityManager manager = null;

        try {
            manager = DBUtil.createEntityManager();

            executor.execute(manager);
        } catch (Exception e) {
            e.printStackTrace();
            throw new LogWorkerException(e.getMessage(), e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }

    interface QueryJob {

        void execute(EntityManager manager);

    }
}
