package com.ef.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DBUtil {

    private static EntityManagerFactory factory;

    private DBUtil() {
        super();
    }

    /**
     * Gets or creates the EntityManagerFactory
     * @return
     */
    public static EntityManagerFactory getFactory() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory("PersistenceUnitLog");
        }

        return factory;
    }

    /**
     * Closes factory
     */
    public static void closeFactory() {
        if (factory != null) {
            factory.close();
        }
    }

    /**
     * Creates a EntityManager
     *
     * @return a new EntityManager
     */
    public static EntityManager createEntityManager() {
        return DBUtil.getFactory().createEntityManager();
    }

}
