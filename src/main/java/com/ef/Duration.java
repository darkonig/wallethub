package com.ef;

public enum Duration {
    HOURLY ("hourly"),
    DAILY ("daily");

    private String name;

    Duration(String name) {
        this.name = name;
    }

    public static Duration getDuration(String duration) throws IllegalArgumentException {
        for (Duration d: Duration.values()) {
            if (d.name.equals(duration)) {
                return d;
            }
        }

        throw new IllegalArgumentException();
    }
}
