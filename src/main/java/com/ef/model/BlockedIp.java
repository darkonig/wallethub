package com.ef.model;

import javax.persistence.*;

@Entity
@Table(name="BLOCKED_IP")
public class BlockedIp {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column (length = 15)
    private String ip;

    @Column (length = 100)
    private String reason;

    public BlockedIp() {
        super();
    }

    public BlockedIp(String ip, String reason) {
        this.ip = ip;
        this.reason = reason;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "{ip='" + ip + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
