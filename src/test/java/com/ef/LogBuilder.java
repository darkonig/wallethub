package com.ef;

import com.ef.model.Log;

import java.util.Date;

public class LogBuilder {
    private Log log;

    public static LogBuilder getBuilder() {
        return new LogBuilder();
    }

    public LogBuilder() {
        this.log = new Log();
    }

    public LogBuilder withDate(Date date) {
        log.setDate(date);
        return this;
    }

    public LogBuilder withIp(String ip) {
        log.setIp(ip);
        return this;
    }

    public LogBuilder withRequest(String request) {
        log.setRequest(request);
        return this;
    }

    public LogBuilder withStatus(int status) {
        log.setStatus(status);
        return this;
    }

    public LogBuilder withUserAgent(String ua) {
        log.setUserAgent(ua);
        return this;
    }

    public Log build() {
        return log;
    }
}
