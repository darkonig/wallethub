package com.ef;

import com.ef.exception.LogWorkerException;
import com.ef.model.BlockedIp;
import com.ef.model.Log;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParserTest {

    private List<Log> logs;

    @Before
    public void setup() throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");

        logs = new ArrayList<>();
        for (int i =0; i<=100; i++) {
            logs.add(LogBuilder.getBuilder()
                    .withDate(DateUtils.addSeconds(df.parse("2017-01-01.15:00:00"), i))
                    .withIp("192.168.0.1")
                    .withRequest("ABC")
                    .withStatus(200)
                    .withUserAgent("Gecko")
                    .build()
            );
        }

        for (int i=0; i<=50; i++) {
            logs.add(LogBuilder.getBuilder()
                    .withDate(DateUtils.addSeconds(df.parse("2017-01-01.15:00:00"), i))
                    .withIp("192.168.1.1")
                    .withRequest("ABC")
                    .withStatus(200)
                    .withUserAgent("Gecko")
                    .build()
            );
        }

        for (int i=0; i<=100; i++) {
            logs.add(LogBuilder.getBuilder()
                    .withDate(DateUtils.addSeconds(df.parse("2017-01-01.15:00:00"), i))
                    .withIp("192.168.1.2")
                    .withRequest("ABC")
                    .withStatus(200)
                    .withUserAgent("Gecko")
                    .build()
            );
        }

        for (int i=0; i<=200; i++) {
            logs.add(LogBuilder.getBuilder()
                    .withDate(DateUtils.addSeconds(df.parse("2017-01-01.15:00:00"), i))
                    .withIp("192.168.1.3")
                    .withRequest("ABC")
                    .withStatus(200)
                    .withUserAgent("Gecko")
                    .build()
            );
        }

        for (int i=0; i<=10; i++) {
            logs.add(LogBuilder.getBuilder()
                    .withDate(DateUtils.addSeconds(df.parse("2017-01-01.15:00:00"), i))
                    .withIp("192.168.1.4")
                    .withRequest("ABC")
                    .withUserAgent("Gecko")
                    .build()
            );
        }
    }

    @Test
    public void processLogTestSuccess() throws ParseException, LogWorkerException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
        Date startDate = df.parse("2017-01-01.15:00:00");

        Parser parser = new Parser();
        List<BlockedIp> blockedIps = parser.getBlockedIps(logs.stream(), startDate, Duration.HOURLY, 100);

        Assert.assertEquals(blockedIps.size(), 4);
    }

    @Test
    public void processLogTestNone() throws ParseException, LogWorkerException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
        Date startDate = df.parse("2017-01-01.15:00:00");

        Parser parser = new Parser();
        List<BlockedIp> blockedIps = parser.getBlockedIps(logs.stream(), startDate, Duration.DAILY, 300);

        Assert.assertEquals(blockedIps.size(), 0);
    }

}
