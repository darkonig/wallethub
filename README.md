# README #
This program parses web server access log file, loads the log to MySQL and checks if a given IP makes more than a certain number of requests for the given duration. 

You can download the jar at release/parser.jar.

### Running ###

```sh
$ java -cp "parser.jar" com.ef.Parser --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 
```

### SQL ###

The sql can be found at: src/main/sql/

### Extra ###
As an extra, here is a java version using only lamda expresion to filter the the log. (Runs fast)
```sh
$ java -cp "parser.jar" com.ef.ParserJava --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 
```